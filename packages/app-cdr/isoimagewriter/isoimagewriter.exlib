# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

if ever at_least 24.01.75 ; then
    require kde-apps kde [ kf_major_version=${major_version} translations=ki18n ]
else
    require kde.org kde [ kf_major_version=${major_version} translations=ki18n ]
fi

SUMMARY="A program to write hybrid ISO files onto a USB disk"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-crypt/gpgme[>=1.8.0]
        kde-frameworks/karchive:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/solid:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
"

if ever at_least 24.01.75 ; then
    DEPENDENCIES+="
        build+run:
            app-crypt/libqgpgme[>=1.8.0][providers:qt6]
    "
else
    DEPENDENCIES+="
        build+run:
            app-crypt/libqgpgme[>=1.8.0][providers:qt5]
    "

    CMAKE_SRC_CONFIGURE_PARAMS+=(
        -DROSA_BRANDING:BOOL=OFF
    )
fi

