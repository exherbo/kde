# Copyright 2015-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde
require utf8-locale python [ blacklist=2 multibuild=false ]

export_exlib_phases pkg_setup

SUMMARY="GTK+ 2 and GTK+ 3 themes built to match KDE's breeze"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-lang/sassc
        dev-python/pycairo[python_abis:*(-)?]
        virtual/pkg-config
    suggestion:
        kde/kde-gtk-config [[ description = [ KDE configuration module to configure appearance of GTK applications ] ]]
"

if ever at_least 5.27.80 ; then
    DEPENDENCIES+="
        build+run:
            kde/breeze:4[>=5.27.80]
    "
else
    DEPENDENCIES+="
        build+run:
            kde/breeze:4[>=5.14.90]
    "
fi

breeze-gtk_pkg_setup() {
    require_utf8_locale
}

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DPython3_EXECUTABLE:PATH=${PYTHON}
)

