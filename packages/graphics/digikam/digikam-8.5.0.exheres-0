# Copyright 2008, 2009, 2010, 2014 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2010-2011 Bo Ørsted Andresen <zlin@exherbo.org>
# Copyright 2012 Xavier Barrachina <xabarci@doctor.upv.es>
# Copyright 2016 Bernd Steinhauser <berniyh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org [ subdir=${PN}/${PV} pn=digiKam ]
require kde [ kf_major_version=6 translations=ki18n ]
require ffmpeg [ with_opt=true option_name=media-player ]
require freedesktop-desktop gtk-icon-cache

SUMMARY="An advanced digital photo management application"
HOMEPAGE="https://www.digikam.org/"

LICENCES="
    ADOBE-DNG  [[ note = [ core/libs/dngwriter ] ]]
    BSD-3      [[ note = [ cmake scripts ] ]]
    CeCILL-2.0 [[ note = [ core/libs/dimg/filters/greycstoration/cimg ] ]]
    FDL-1.2    [[ note = [ documentation ] ]]
    GPL-2 LGPL-2.1
    (
        || ( LGPL-2.1 CDDL-1.0 )
    ) [[ note = [ libraw ] ]]
    (
        GPL-2 GPL-3
    ) [[ note = [ libraw's demosaic files ] ]]
"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    calendar [[ description = [ Calendar support ] ]]
    contacts [[ description = [ Access kdepim/kmail contacts ] requires = calendar ]]
    doc
    gphoto2 [[ description = [ Support for PTP access ] ]]
    heif [[ description = [ Support for the H(igh) E(fficiency) I(mage) F(ile) F(ormat) ] ]]
    jpeg2000
    jpegxl
    media-player [[ description = [ Play non-still media using qtmultimedia ] ]]
    mysql [[ description = [ Use mysql instead of sqlite for the database ] ]]
    scanner [[ description = [ Aquire images from scanners via sane ] ]]
    semantic-desktop [[ description = [ Index files using kfilemetadata ] ]]

    ( linguas:
        af ar az be bg bn br bs ca cs csb cy da de el en_GB eo es et eu fa fi
        fo fr fy ga gl ha he hi hr hsb hu id is it ja ka kk km ko ku lb lo lt
        lv mi mk mn ms mt nb nds ne nl nn nso oc pa pl pt pt_BR ro ru rw se sk
        sl sq sr sr@Latn ss sv ta te tg th tr tt uk uz uz@cyrillic ven vi wa
        xh zh_CN zh_HK zh_TW zu
    )
    ( providers: graphicsmagick imagemagick ) [[
        *description = [ Software suites to create, edit, and compose bitmap images ]
        number-selected = at-least-one
    ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

RESTRICT="test"

KF6_MIN_VERSION=5.95.0
QT_MIN_VERSION=6.4.0

# NOTE: digikam unfortunately bundles libpgf-7.15.32
DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:6[>=${KF6_MIN_VERSION}]
        sys-devel/bison
        sys-devel/flex[>=2.6.0-r1]
        sys-devel/gettext
        virtual/pkg-config
        doc? ( app-doc/doxygen[>=1.8.0] )
    build+run:
        dev-libs/boost
        dev-libs/expat[>=2.1.0] [[ note = [ For DNGConverter: XMP SDK need Expat library to compile ] ]]
        dev-libs/glib:2 [[ note = [ For bundled liblqr ] ]]
        dev-libs/libglvnd [[ note = [ For Presentation tool ] ]]
        dev-libs/libxml2:2.0[>=2.7.0]
        dev-libs/libxslt[>=1.1.0]
        graphics/exiv2:=[>=0.27.1]
        kde-frameworks/kcompletion:6[>=${KF6_MIN_VERSION}]
        kde-frameworks/kconfig:6[>=${KF6_MIN_VERSION}]
        kde-frameworks/kconfigwidgets:6[>=${KF6_MIN_VERSION}]
        kde-frameworks/kcoreaddons:6[>=${KF6_MIN_VERSION}]
        kde-frameworks/kiconthemes:6[>=${KF6_MIN_VERSION}] [[ note = [ desktop integration ] ]]
        kde-frameworks/kio:6[>=${KF6_MIN_VERSION}] [[ note = [ desktop integration ] ]]
        kde-frameworks/knotifications:6[>=${KF6_MIN_VERSION}]
        kde-frameworks/knotifyconfig:6[>=${KF6_MIN_VERSION}]
        kde-frameworks/kservice:6[>=${KF6_MIN_VERSION}]
        kde-frameworks/kwidgetsaddons:6[>=${KF6_MIN_VERSION}]
        kde-frameworks/kxmlgui:6[>=${KF6_MIN_VERSION}]
        kde-frameworks/solid:6[>=${KF6_MIN_VERSION}]
        kde-frameworks/sonnet:6[>=${KF6_MIN_VERSION}]
        kde-frameworks/threadweaver:6[>=${KF6_MIN_VERSION}] [[ note = [ Panorama tool ] ]]
        media-libs/lcms2[>=2.1]
        media-libs/lensfun[>=0.2.6] [[ note = [ To support LensCorrection editor ] ]]
        media-libs/libpng:=[>=1.2.7]
        media-libs/opencv[>=3.3.0][dnn][qt5(-)]
        media-libs/tiff:=[>=3.8.2]
        sci-libs/eigen:3
        sys-libs/libgomp:=
        sys-libs/zlib
        x11-libs/libX11[>=1.1.5] [[ note = [ For Monitor profiles ] ]]
        x11-libs/qtbase:6[>=${QT_MIN_VERSION}][gui][sql][sqlite]
        x11-libs/qtnetworkauth:6[>=${QT_MIN_VERSION}]
        x11-libs/qtscxml:6[>=${QT_MIN_VERSION}]
        x11-libs/qtsvg:6[>=${QT_MIN_VERSION}]
        x11-libs/qtwebengine:6[>=${QT_MIN_VERSION}]
        calendar? ( kde-frameworks/kcalendarcore:6[>=5.89.0] )
        contacts? (
            kde-frameworks/akonadi-contact:6[>=5.19.0]
            kde-frameworks/kcontacts:6
        )
        gphoto2? (
            dev-libs/libusb:1
            media-libs/libgphoto2[>=2.4.0]
        )
        heif? (
            media-libs/libheif[>=1.6.0]
            media-libs/x265:=[>=2.2]
        )
        jpeg2000? ( media-libs/jasper[>=1.7.0] )
        jpegxl? ( media-libs/libjxl:=[>=0.7] )
        media-player? ( x11-libs/qtmultimedia:6[>=${QT_MIN_VERSION}][ffmpeg_abis:*(-)?] )
        mysql? ( virtual/mysql )
        providers:graphicsmagick? ( media-gfx/GraphicsMagick[imagemagick][>=1.0.6] )
        providers:imagemagick? ( media-gfx/ImageMagick[>=5.5.4] )
        providers:ijg-jpeg? ( media-libs/jpeg:=[>=8] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        scanner? ( kde/libksane:6[>=21.12.0] )
        semantic-desktop? ( kde-frameworks/kfilemetadata:6[>=${KF6_MIN_VERSION}] )
    run:
        media-gfx/enblend-enfuse[>=3.0]
    suggestion:
        media-gfx/hugin[>=0.8] [[ description = [ Combine images to panorama views ] ]]
        kde/ffmpegthumbs:4 [[ description = [ For thumbnails of videos ] ]]
        kde-frameworks/kimageformats:6 [[ description = [ Support for additional image formats ] ]]
"

CMAKE_SOURCE=${WORKBASE}/${PNV}

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_FIND_ROOT_PATH:PATH="$(ffmpeg_alternatives_prefix);/usr/$(exhost --target)"
    -DBUILD_WITH_CCACHE:BOOL=FALSE
    -DBUILD_WITH_QT6:BOOL=TRUE
    -DDIGIKAMSC_COMPILE_DIGIKAM:BOOL=TRUE
    -DDIGIKAMSC_COMPILE_PO:BOOL=TRUE
    -DENABLE_APPSTYLES:BOOL=TRUE
    -DENABLE_DBUS:BOOL=TRUE
    -DENABLE_GEOLOCATION:BOOL=TRUE
    -DENABLE_INTERNALMYSQL:BOOL=FALSE
    -DENABLE_KIO:BOOL=TRUE
    -DENABLE_SHOWFOTO:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS=(
    'calendar KF6CalendarCore'
    'doc Doxygen'
    'gphoto2 Gphoto2'
    'heif Libheif'
    'jpeg2000 Jasper'
    'jpegxl Libjxl'
    'scanner KSaneWidgets6'
)

CMAKE_SRC_CONFIGURE_OPTION_ENABLES=(
    'contacts AKONADICONTACTSUPPORT'
    'media-player MEDIAPLAYER'
    'media-player QTMULTIMEDIA'
    'mysql MYSQLSUPPORT'
    'semantic-desktop KFILEMETADATASUPPORT'
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
)

pkg_setup() {
    ffmpeg_pkg_setup
}

src_compile() {
    default

    option doc && emake doc
}

src_install() {
    kde_src_install

    if option doc; then
        insinto /usr/share/doc/${PNVR}
        doins -r api/html
    fi
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

