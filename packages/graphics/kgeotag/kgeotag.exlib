# Copyright 2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde.org [ group_or_user=graphics ]
require kde [ kf_major_version=${major_version} translations=ki18n ]
require gtk-icon-cache

SUMMARY="A Free/Libre Open Source photo geotagging program"

LICENCES="
    BSD-3 [[ note = [ cmake scripbs ] ]]
    CC0 [[ note = [ dotfiles, desktop and appstream files ] ]]
    CCPL-Attribution-ShareAlike-4.0 [[ note = [ icons, README ] ]]
    GPL-3
"
SLOT="0"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:${major_version}[>=${KF5_MIN_VER}]
    build+run:
        kde-frameworks/kconfigwidgets:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=5.12.0]
"

if ever at_least 1.6.80 ; then
    DEPENDENCIES+="
        build+run:
            kde/marble:4[>=24.11.80]
            kde-frameworks/kcolorscheme:6[>=${KF5_MIN_VER}]
            kde-frameworks/libkexiv2:6[>=24.05.0] [[ note = [ aka 5.1.0 ] ]]
    "
else
    DEPENDENCIES+="
        build+run:
            kde/marble:4[>=21.12.0&<24.11]
            kde-frameworks/libkexiv2:5[>=5.0.0]
    "
fi

