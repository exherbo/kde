# Copyright 2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps kde [ kf_major_version=${major_version} translations=ki18n ]
require gtk-icon-cache freedesktop-desktop

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="OTP client for Plasma Mobile and Desktop"

LICENCES="
    BSD-2 [[ note = [ cmake scripts ] ]]
    GPL-3
"
SLOT="0"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/libsodium[>=1.0.16]
        kde-frameworks/kdbusaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}]
        x11-libs/qtsvg:${major_version}[>=${QT_MIN_VER}]
"

if ever at_least 24.11.80 ; then
    DEPENDENCIES+="
        build+run:
            kde-frameworks/kcoreaddons:6[>=${KF5_MIN_VER}]
            kde-frameworks/qqc2-desktop-style:6[>=${KF5_MIN_VER}]
        run:
            kde-frameworks/kirigami:6[>=${KF5_MIN_VER}]
    "
else
    DEPENDENCIES+="
        build+run:
            x11-libs/qtquickcontrols2:5[>=${QT_MIN_VER}]
        run:
            kde-frameworks/kirigami:2[>=${KF5_MIN_VER}]
    "
fi

keysmith_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

keysmith_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

