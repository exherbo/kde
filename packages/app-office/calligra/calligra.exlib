# Copyright 2008, 2009, 2010, 2014 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2008, 2009, 2010 Bo Ørsted Andresen <zlin@exherbo.org>
# Copyright 2011 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2013-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde.org kde [ kf_major_version=${major_version} translations='ki18n' ]
require freedesktop-desktop freedesktop-mime gtk-icon-cache
require flag-o-matic

export_exlib_phases src_configure pkg_postinst pkg_postrm

SUMMARY="Collection of office applications for the K Desktop Environment"
DESCRIPTION="
Office productivity:
* Words: Word processor
* Author: Supports a writer in the creation of books (based on Words)
* Sheets: Spreadsheet calculator
Graphics:
* Karbon: Vector graphics
* Gemini: Calligra adjusted for 2-in-1 devices
* Stage: Presentation program (currently unmaintained, disabled)
* Braindump: Mindmapping tool (currently unmaintained, disabled)
"

LICENCES="GPL-2 LGPL-2 FDL-1.2"
SLOT="2"
MY_KDE_PARTS="gemini karbon sheets stage words"
# braindump - unmaintained and disabled for a default build

MYOPTIONS+="
    apple-keynote  [[ description = [ Support for importing files from Apple's Keynote ] ]]
    charts         [[ description = [ Support for embedding charts and diagrams ] ]]
    encrypted      [[ description = [ Support for encrypted OpenDocument files in Words ] ]]
    eps            [[ description = [ EPS (Encapsulated PostScript) import filter for Karbon ] ]]
    msworks        [[ description = [ Support for Microsoft Works files in Words ] ]]
    okular         [[ description = [ Build plugin for Okular supporting ODP and MS PPT/PPT documents ] ]]
    pdf            [[ description = [ Support for PDF (Portable Document Format) import in Karbon ] ]]
    spacenavigator [[ description = [ Space navigator 3D mouse device plugin ] ]]
    wordperfect    [[ description = [ WordPerfect Document support for Words and Graphics support for Karbon ] ]]

    ( kde_parts: ( ${MY_KDE_PARTS} ) [[ number-selected = at-least-one ]] )
    wordperfect? ( ( kde_parts: karbon words ) [[ number-selected = at-least-one ]] )

    apple-keynote  [[ requires = [ kde_parts: stage ] ]]
    eps            [[ requires = [ kde_parts: karbon ] ]]
    msworks        [[ requires = [ kde_parts: words ] ]]
    pdf            [[ requires = [ kde_parts: karbon ] ]]
"

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES+="
    build:
        kde-frameworks/kdoctools:${major_version}
        sys-devel/gettext
    build+run:
        dev-lang/perl:*
        dev-libs/boost
        kde-frameworks/karchive:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcmutils:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcodecs:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/knotifyconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/sonnet:${major_version}[>=${KF5_MIN_VER}]
        media-libs/fontconfig [[ note = [ Required to handle exact font size ] ]]
        media-libs/freetype:2 [[ note = [ Required to handle exact font size ] ]]
        media-libs/lcms2[>=2.4] [[ note = [ Required for color management ] ]]
        media-libs/libpng:=
        sys-libs/zlib
        x11-dri/glu
        x11-dri/mesa [[ note = [ Required for opengl support in Flake ] ]]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}][gui]
        x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}]
        x11-libs/qtsvg:${major_version}[>=${QT_MIN_VER}]
        x11-libs/qttools:${major_version}[>=${QT_MIN_VER}]
        x11-misc/shared-mime-info[>=1.3] [[ note = [ filters/libmsooxml/ ] ]]
        charts? ( kde/kdiagram:0[>=2.7.0] )
        kde_parts:gemini? ( dev-scm/libgit2:= )
        kde_parts:karbon? (
            eps? ( media-gfx/pstoedit [[ note = [ Required for eps import (PS/PDF to SVG) ] ]] )
        )
        kde_parts:sheets? (
            sci-libs/eigen:3[>=3.0]
            sci-libs/gsl[>=1.7]
            x11-libs/qtbase:${major_version}[sql] [[ note = [ Optional for Sheets database connection ] ]]
        )
        kde_parts:stage? (
            apple-keynote? (
                office-libs/libetonyek[>=0.1]
                office-libs/librevenge
            )
        )
        kde_parts:words? (
            wordperfect? (
                office-libs/libodfgen[>=0.1]
                office-libs/librevenge
                office-libs/libwpd[>=0.10]
                office-libs/libwpg[>=0.3]
            )
            msworks? (
                office-libs/libodfgen[>=0.1]
                office-libs/librevenge
                office-libs/libwps[>=0.4]
            )
        )
        spacenavigator? ( sci-libs/libspnav )
    test:
        kde-frameworks/threadweaver:${major_version}[>=${KF5_MIN_VER}]
"

if ever at_least 3.3.89 ; then
    MYOPTIONS+="
        openexr [[ description = [ Support for OpenEXR, a high precision file format for use in computer imaging applications ] ]]
        ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
    "
    DEPENDENCIES+="
        build+run:
            kde-frameworks/kcolorscheme:${major_version}[>=${KF5_MIN_VER}]
            media-libs/phonon[providers:qt6] [[ note = [ Stage event actions and videoshape plugin ] ]]
            sys-auth/qtkeychain[providers:qt6]
            charts? ( kde/kdiagram:${major_version}[>=2.7.0] )
            encrypted? ( app-crypt/qca:2[>=2.1.0][providers:qt6] [[ note = [ Support encrypted odf and ooxml (MS Office 2007) files ] ]] )
            kde_parts:karbon? (
                pdf? ( app-text/poppler[>=22.02.0][qt6] [[ note = [ Karbon PDF import filter ] ]] )
            )
            okular? ( kde/okular:4[>=24.04.0] )
            openexr? (
                media-libs/imath
                media-libs/openexr[>=3]
            )
            providers:libressl? ( dev-libs/libressl:= )
            providers:openssl? ( dev-libs/openssl:= )
    "
else
    DEPENDENCIES+="
        build+run:
            kde-frameworks/kactivities:${major_version}[>=${KF5_MIN_VER}] [[ note = [ could be optional ] ]]
            kde-frameworks/kcodecs:${major_version}[>=${KF5_MIN_VER}]
            kde-frameworks/kdelibs4support:${major_version}[>=${KF5_MIN_VER}]
            kde-frameworks/khtml:${major_version}[>=${KF5_MIN_VER}]         [[ note = [ could be optional ] ]]
            kde-frameworks/kinit:${major_version}[>=${KF5_MIN_VER}] [[ note = [ kf5_add_kdeinit_executable cmake macro ] ]]
            kde-frameworks/kross:${major_version}[>=${KF5_MIN_VER}]
            kde-frameworks/kwallet:${major_version}[>=${KF5_MIN_VER}]
            media-libs/phonon[providers:qt5] [[ note = [ Stage event actions and videoshape plugin ] ]]
            x11-libs/libX11
            x11-libs/qtx11extras:${major_version}[>=${QT_MIN_VER}]
            encrypted? ( app-crypt/qca:2[>=2.1.0][providers:qt5] [[ note = [ Support encrypted odf and ooxml (MS Office 2007) files ] ]] )
            charts? ( kde/kdiagram:0[>=2.7.0] )
            kde_parts:karbon? (
                pdf? ( app-text/poppler[>=0.83.0][qt5] [[ note = [ Karbon PDF import filter ] ]] )
            )
            okular? ( kde/okular:4[>=16.11.0&<24.01.85] )
    "
fi

# Tests need X, last checked: 2.7.2
RESTRICT="test"

# FIXME: Is the apidox custom target useful now?

calligra_src_configure() {
    if [[ $(exhost --target) == *-musl* ]] ; then
        # lex.yy.c:5973:48: error: implicit declaration of function 'fileno';
        append-flags "-D_GNU_SOURCE"
    fi

    local p cmakeparams=(
        -DCMAKE_DISABLE_FIND_PACKAGE_LibEtonyek:BOOL=TRUE
        -DCMAKE_DISABLE_FIND_PACKAGE_LibRevenge:BOOL=TRUE
        -DCMAKE_DISABLE_FIND_PACKAGE_Qt${major_version}Declarative:BOOL=TRUE # Deprecated, needed for gemini
        -DCMAKE_DISABLE_FIND_PACKAGE_Qt${major_version}WebKit:BOOL=TRUE
        -DLIBEMF_DEBUG:BOOL=FALSE
        -DPACKAGERS_BUILD:BOOL=FALSE # Support multiple CPU arches in one binary - if Vc is present, see README.PACKAGERS
        -DRELEASE_BUILD:BOOL=TRUE
        -DWITH_Cauchy:BOOL=FALSE # Matlab/Octave formulas in words/stage - unwritten, no releases yet
        -DWITH_Iconv:BOOL=TRUE # Required for MS .doc support
        -DWITH_LCMS2:BOOL=TRUE
        -DWITH_LibVisio:BOOL=FALSE   # visio option -> flow (unported)
        -DWITH_Marble:BOOL=FALSE # depends on rdf (Soprano) -> see below
        $(cmake_with encrypted Qca-qt${major_version})
        $(cmake_with eps PstoeditSvgOutput)
        $(cmake_with kde_parts:sheets Eigen3)
        $(cmake_with kde_parts:sheets GSL)
        $(cmake_with msworks LibRevenge)
        $(cmake_with msworks LibWps)
        $(cmake_with okular Okular${major_version})
        $(cmake_with pdf Poppler)
        $(cmake_with pdf PopplerXPDFHeaders)
        # Commented out: "push for released (and maintained) Qt5 version of
        # Soprano" - Needed for marble option
        #$(cmake_with rdf Soprano)
        $(cmake_with spacenavigator Spnav)
        $(cmake_with wordperfect LibRevenge)
        $(cmake_with wordperfect LibWpd)
        $(cmake_with wordperfect LibWpg)
        $(expecting_tests -DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE)
    )

    if ever at_least 3.3.89 ; then
        cmakeparams+=(
            $(cmake_disable_find openexr Imath)
            $(cmake_with charts KChartQt6)
            $(cmake_with kde_parts:gemini LibGit2)
        )
    else
        cmakeparams+=(
            -DGHNS:BOOL=FALSE # considered broken by upstream atm, needs kde/attica
            -DWITH_OpenEXR:BOOL=FALSE # incompatible with OpenEXR >= 3
            $(cmake_with charts KChart)
            $(cmake_with kde_parts:gemini Libgit2)
        )
    fi

    if option kde_parts:words && option wordperfect || option msworks ; then
        cmakeparams+=( -DWITH_LibOdfGen:BOOL=TRUE )
    else
        cmakeparams+=( -DWITH_LibOdfGen:BOOL=FALSE )
    fi

    for p in ${MY_KDE_PARTS}; do
        cmakeparams+=( $(cmake_build kde_parts:${p}) )
    done

    if ever at_least 3.3.89 ; then
        ecmake $(kf6_shared_cmake_params) "${cmakeparams[@]}"
    else
        ecmake $(kf5_shared_cmake_params) "${cmakeparams[@]}"
    fi
}

calligra_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

calligra_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

