# Copyright 2015-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org [ subdir="${PN}/src" ] kde [ translations=qt ]
require gtk-icon-cache

SUMMARY="A framework for the creation and generation of reports in multiple formats"
DESCRIPTION="
The KReport framework implements reporting functionality for creation of reports
in MS Access style. They are also similar to SAP Crystal Reports and FileMaker
reports.
Reports can be created interactively and programmatically. They can be previewed on screen,
printed, and saved in a variety of formats such as HTML, PDF and OpenDocument.
Reports of this kind offer a way to view, format, and summarize the information.
For example a simple report of contact phone numbers can be prepared, or a more complex
report on sales for different products, regions, and periods of time."

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] FDL-1.2 LGPL-2"
SLOT="0"
MYOPTIONS="
    doc
    maps      [[ description = [ Include maps into reports ] ]]
    scripting [[ description = [ Scripting support using JavaScript language ] ]]
    web       [[ description = [ Web browser element for reports ] ]]
"

KF5_MIN_VER=5.16.0

DEPENDENCIES="
    build:
        dev-lang/python:*
        doc? (
            app-doc/doxygen
            dev-lang/perl:*
            x11-libs/qttools:5 [[ note = [ qhelpgenerator ] ]]
        )
    build+run:
        dev-libs/kproperty[>=3.0.0]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=5.4.0][gui]
        maps? ( kde/marble:4[<24.11] )
        scripting? ( x11-libs/qtdeclarative:5 )
"

# 2 of 4 tests need a running X server
RESTRICT="test"

# Commented out atm: chart plugin
CMAKE_SRC_CONFIGURE_PARAMS+=(
    # Enabling them does not install anything so disable building them
    -DBUILD_EXAMPLES:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'scripting KREPORT_SCRIPTING'
)
CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'doc QCH' )
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'maps Marble'
    'web Qt5WebKitWidgets'
)

