# Copyright 2008, 2009, 2010, 2012, 2013, 2014, 2016 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2015 Bernd Steinhauser <berniyh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# currently fails with either ki18n or qt, last checked: 3.9.3
require kde.org [ subdir=${PN}/${PV} ] kde [ translations=none ]
require ffmpeg [ with_opt=true option_name=chromaprint ]
require freedesktop-desktop gtk-icon-cache

SUMMARY="A KDE music tagger"
DESCRIPTION="
If you want to easily tag multiple MP3, Ogg/Vorbis, FLAC, MPC, MP4/AAC, MP2,
Speex, TrueAudio and WavPack files (e.g. full albums) without typing the same
information again and again and have control over both ID3v1 and ID3v2 tags,
then Kid3 is the program you are looking for.
"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    chromaprint [[ description = [ Support for import from MusicBrainz Fingerprint ] ]]
    id3         [[ description = [ Support for ID3v1 and ID3v2 tags ] ]]
"

DEPENDENCIES="
    build:
        app-text/docbook-xsl-stylesheets
        dev-lang/perl:*
        dev-libs/libxslt
        kde-frameworks/kdoctools:5
        chromaprint? ( virtual/pkg-config )
    build+run:
        kde-frameworks/kconfig:5
        kde-frameworks/kconfigwidgets:5
        kde-frameworks/kcoreaddons:5
        kde-frameworks/kio:5
        kde-frameworks/kwidgetsaddons:5
        kde-frameworks/kxmlgui:5
        media-libs/flac:= [[ note = [ Need Flac++, --enable-cxx. ] ]]
        media-libs/libogg
        media-libs/libvorbis
        media-libs/taglib:=[>=1.4]
        sys-libs/readline:=
        sys-libs/zlib
        x11-libs/qtbase:5[gui]
        x11-libs/qtdeclarative:5
        x11-libs/qtmultimedia:5
        x11-libs/qttools:5
        chromaprint? ( media-libs/chromaprint )
        id3? ( media-libs/id3lib )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-3.9.4-Use-CMAKE_INSTALL_FULL_DATAROOTDIR.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_FIND_ROOT_PATH:PATH="$(ffmpeg_alternatives_prefix);/usr/$(exhost --target)"
    -DBUILD_WITH_QT6:BOOL=FALSE
    -DWITH_APPS:STRING="CLI;KDE"
    -DWITH_DBUS:BOOL=TRUE
    # gstreamer can be used instead of ffmpeg for chromaprint decoding, seems pointless to add
    # options for that, so if chromaprint is enabled, force ffmpeg on
    -DWITH_GSTREAMER:BOOL=FALSE
    -DWITH_MP4V2:BOOL=FALSE
    -DWITH_MULTIMEDIA:BOOL=FALSE
    -DWITH_NO_MANCOMPRESS:BOOL=TRUE
    # QAudioDecoder from QtMultimedia[>=5] can also be used for chromaprint
    # decoding. Given that we build the KDE version we cannot use that with
    # KDE-4.x.
    -DWITH_QAUDIODECODER:BOOL=FALSE
    -DWITH_QML:BOOL=TRUE
    -DWITH_READLINE:BOOL=TRUE
    -DWITH_TAGLIB:BOOL=TRUE
    -DWITH_VORBIS:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTION_WITHS=(
    'id3 ID3LIB'
    'chromaprint CHROMAPRINT'
    'chromaprint FFMPEG'
    'chromaprint CHROMAPRINT_FFMPEG'
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
)

