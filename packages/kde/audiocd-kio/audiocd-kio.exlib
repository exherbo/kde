# Copyright 2012 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps kde [ kf_major_version=${major_version} translations='ki18n' ]

SUMMARY="KIO worker for accessing audio CDs"
HOMEPAGE+=" https://multimedia.kde.org/"

LICENCES="GPL-2 FDL-1.2 BSD-3 [[ note = [ cmake scripts ] ]]"
MYOPTIONS="flac vorbis"

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:${major_version}[>=${KF5_MIN_VER}]
    build+run:
        kde-frameworks/kcmutils:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        media/cdparanoia
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
        flac? ( media-libs/flac:=[>=1.1.2] )
        vorbis? (
            media-libs/libogg
            media-libs/libvorbis
        )
    suggestion:
        media-sound/lame [[ description = [ Support MP3 in the AudioCD kioslave ] ]]
        media-sound/opus-tools [[ description = [ Support for Opus in the AudioCD kioslave ] ]]
"

if ever at_least 24.11.80 ; then
    DEPENDENCIES+="
        build+run:
            kde/libkcddb:6
            kde/libkcompactdisc:6
            kde-frameworks/kwidgetsaddons:6[>=${KF5_MIN_VER}]
    "
else
    DEPENDENCIES+="
        build+run:
            kde/libkcddb:5[>=22.03.80]
            kde/libkcompactdisc:5[>=16.11.80]
    "
fi

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    FLAC
    'vorbis OggVorbis'
)

