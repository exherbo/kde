# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations='ki18n' ]
require xdummy [ phase=test ]

myexparam kf5_min_ver
myexparam qt_min_ver

export_exlib_phases src_test

SUMMARY="Control Panel for your system firewall"

LICENCES="
    BSD-3 [[ note = [ cmake scripts ] ]]
    CC0
    FSFAP
    GPL-2
    GPL-3
    LGPL-3.0
"
SLOT="0"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3]
        kde-frameworks/kpackage:5[>=${KF5_MIN_VER}]
    build+run:
        kde-frameworks/kauth:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
    run:
        net-firewall/firewalld
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DBUILD_FIREWALLD_BACKEND:BOOL=TRUE
    -DBUILD_UFW_BACKEND:BOOL=FALSE
)

plasma-firewall_src_test() {
    xdummy_start
    cmake_src_test
    xdummy_stop
}

