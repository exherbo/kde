# Copyright 2020-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps kde [ kf_major_version=${major_version} translations=ki18n ]
require gtk-icon-cache
ever at_least 24.11.80 && require xdummy [ phase=test ] test-dbus-daemon

export_exlib_phases src_test

SUMMARY="A convergent RSS/Atom feed reader"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build+run:
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/syndication:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}][sql]
        x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}]
"

if ever at_least 24.11.80 ; then
    DEPENDENCIES+="
        run:
            kde/kirigami-addons:6[>=0.6]
            kde-frameworks/kcolorscheme:6[>=${KF5_MIN_VER}]
            kde-frameworks/kirigami:${major_version}[>=${KF5_MIN_VER}]
    "
else
    DEPENDENCIES+="
        build+run:
            x11-libs/qtquickcontrols2:5[>=${QT_MIN_VER}]
        run:
            kde/kirigami-addons:0[>=0.6]
            kde-frameworks/kirigami:2[>=${KF5_MIN_VER}]
    "
fi

alligator_src_test() {
     if ever at_least 24.11.80 ; then
        xdummy_start
        test-dbus-daemon_run-tests_src_test
        xdummy_stop
    else
        cmake_src_test
     fi
}

