# Copyright 2014 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2015-2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf5_min_ver
myexparam qt_min_ver

require plasma kde [ kf_major_version=5 translations='ki18n' ]
require gtk-icon-cache

SUMMARY="Artwork, styles and assets for the Breeze style for the Plasma Desktop"

LICENCES="GPL-2 LGPL-3 [[ note = [ icons ] ]]"
SLOT="4"
# wallpapers need 22 of 28 MB here, a bit much if you only want the style.
MYOPTIONS="
    wallpapers [[ description = [ Installs the Breeze default wallpapers ] ]]
"

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        kde/kdecoration:${SLOT} [[ note = [ could be optional with 5.6.x ] ]]
        kde-frameworks/frameworkintegration:5[>=${KF5_MIN_VER}] [[ note = [ provides KF5Style ] ]]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
    run:
        kde-frameworks/breeze-icons:5
        kde-frameworks/kirigami:2[>=${KF5_MIN_VER}]
    post:
        kde/kde-cli-tools:4 [[ note = [ kcmshell5 ] ]]
    suggestion:
        x11-themes/breeze-gtk [[ description = [ Matching theme for gtk+:2 and gtk+:3 ] ]]
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DWITH_DECORATIONS:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_WITHS+=( WALLPAPERS )

