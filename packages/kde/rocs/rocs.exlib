# Copyright 2011 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2015-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf5_min_ver
myexparam qt_min_ver

require kde-apps kde [ translations='ki18n' ]
require xdummy [ phase=test ] test-dbus-daemon
require freedesktop-desktop gtk-icon-cache

export_exlib_phases src_test pkg_postinst pkg_postrm

SUMMARY="KDE EDU: Graph Theory IDE"

LICENCES="GPL-2 FDL-1.2 LGPL-2 LGPL-2.1 BSD-2 [[ note = [ cmake scripts ] ]]"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        dev-libs/boost[>=1.49.0] [[ note = [ rocs can be compiled with boost[>=1.43], but 1.49.0
                is necessary for DOT file support ] ]]
        dev-libs/grantlee:5[>=5.0.0]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktexteditor:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtscript:5[>=${QT_MIN_VER}][tools]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        x11-libs/qtxmlpatterns:5[>=${QT_MIN_VER}]
    run:
        kde/kate:${SLOT}
        x11-libs/qtquickcontrols:5[>=${QT_MIN_VER}]
"

# Test appear to be a tad to fragile and unmaintained to enabled them for now
RESTRICT="test"

# Skip tests which also fail on b.k.o, e.g.:
# https://build.kde.org/job/Applications/job/rocs/job/stable-kf5-qt5%20SUSEQt5.12/17/console
CMAKE_SRC_TEST_PARAMS+=(
    -E '(TestTgfFileFormat|TestRocs1FileFormat|TestRocs2FileFormat|graphtheory-test_graphoperations)'
)

rocs_src_test() {
    xdummy_start

    test-dbus-daemon_run-tests cmake_src_test

    xdummy_stop
}

rocs_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

rocs_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

