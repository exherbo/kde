# Copyright 2014-2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf5_min_ver
myexparam qt_min_ver

require plasma kde [ kf_major_version=5 translations='ki18n' ]

export_exlib_phases pkg_postinst

SUMMARY="KDE Power Management System"

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] GPL-2"
SLOT="4"
MYOPTIONS="
    ddcci    [[ description = [ Set monitor settings over DDC/CI channel (not recommended yet) ] ]]
    wireless [[ description = [ Support turning of wireless devices to save energy ] ]]
    ( providers: eudev systemd ) [[
        *description = [ udev provider ]
        number-selected = exactly-one
    ]]
"

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5
        virtual/pkg-config
    build+run:
        kde/plasma-workspace:4[>=$(ever range 1-3)]
        kde-frameworks/kactivities:5[>=${KF5_MIN_VER}]
        kde-frameworks/kauth:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kglobalaccel:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kidletime:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kirigami:2[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifyconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/libkscreen:5
        kde-frameworks/solid:5[>=${KF5_MIN_VER}]
        sys-libs/libcap
        x11-libs/libxcb
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtx11extras:5[>=5.3.0]
        ddcci? ( sys-apps/ddcutil:= )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
        wireless? (
                kde-frameworks/bluez-qt:5[>=${KF5_MIN_VER}]
                kde-frameworks/networkmanager-qt:5[>=${KF5_MIN_VER}]
        )
    run:
        kde/kde-cli-tools:4 [[ note = [ kcmshell5 ] ]]
        sys-apps/upower
    recommendation:
        sys-apps/power-profiles-daemon [[
            description = [ Support switching between power profiles ]
        ]]
"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'wireless KF5BluezQt'
    'wireless KF5NetworkManagerQt'
)
CMAKE_SRC_CONFIGURE_OPTION_HAVES+=( 'ddcci DDCUTIL' )

powerdevil-5_pkg_postinst() {
    # "Needed for scheduled wakeup which can wake from suspend"
    # Doesn't carry over from src_install so we need to set this manually
    edo setcap CAP_WAKE_ALARM=+ep /usr/$(exhost --target)/libexec/org_kde_powerdevil
}

