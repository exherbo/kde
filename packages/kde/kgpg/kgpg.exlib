# Copyright 2011,2014-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'kdeutils.exlib', which is:
#     Copyright 2008-2011 Bo Ørsted Andresen

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps kde [ kf_major_version=${major_version} translations='ki18n' ]
require freedesktop-desktop gtk-icon-cache
require xdummy [ phase=test ] test-dbus-daemon

export_exlib_phases src_test pkg_postinst pkg_postrm

SUMMARY="Graphical interface for GnuPG"
HOMEPAGE="https://apps.kde.org/${PN}/"
DESCRIPTION="
With KGpg you will be able to encrypt and decrypt your files and emails,
allowing much more secure communications. A mini howto on encryption with gpg
is available on gnupg's web site.
"

LICENCES="GPL-2"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        app-crypt/gpgme [[ note = [ only headers are used ] ]]
        kde-frameworks/kdoctools:${major_version}[>=${KF5_MIN_VER}]
        virtual/pkg-config
    build+run:
        kde-frameworks/akonadi-contact:5
        kde-frameworks/karchive:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcodecs:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcontacts:${major_version}
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}][gui]
    run:
        app-crypt/gnupg
"

if ever at_least 24.04.80 ; then
    DEPENDENCIES+="
        build+run:
            kde-frameworks/kstatusnotifieritem:${major_version}[>=${KF5_MIN_VER}]
    "
fi

# Seems to be a bit flaky
CMAKE_SRC_TEST_PARAMS+=( -E kgpg-import )

kgpg_src_test() {
    esandbox allow_net "unix:${TEMP%/}/kgpg-*/.gnupg/S.gpg-agent"
    esandbox allow_net --connect "unix:${TEMP%/}/kgpg-*/.gnupg/S.gpg-agent"
    esandbox allow_net "unix:${TEMP%/}/kgpg-*/.gnupg/S.gpg-agent.browser"
    esandbox allow_net "unix:${TEMP%/}/kgpg-*/.gnupg/S.gpg-agent.extra"
    esandbox allow_net "unix:${TEMP%/}/kgpg-*/.gnupg/S.gpg-agent.ssh"
    esandbox allow_net "unix:${TEMP%/}/kgpg-*/.gnupg/S.scdaemon"

    xdummy_start

    test-dbus-daemon_run-tests cmake_src_test

    xdummy_stop

    esandbox disallow_net "unix:${TEMP%/}/kgpg-*/.gnupg/S.scdaemon"
    esandbox disallow_net "unix:${TEMP%/}/kgpg-*/.gnupg/S.gpg-agent.ssh"
    esandbox disallow_net "unix:${TEMP%/}/kgpg-*/.gnupg/S.gpg-agent.extra"
    esandbox disallow_net "unix:${TEMP%/}/kgpg-*/.gnupg/S.gpg-agent.browser"
    esandbox disallow_net --connect "unix:${TEMP%/}/kgpg-*/.gnupg/S.gpg-agent"
    esandbox disallow_net "unix:${TEMP%/}/kgpg-*/.gnupg/S.gpg-agent"
}

kgpg_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

kgpg_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

