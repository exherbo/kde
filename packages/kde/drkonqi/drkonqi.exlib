# Copyright 2017-2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf5_min_ver
myexparam qt_min_ver

require plasma kde [ kf_major_version=6 translations='ki18n' ]
require python [ blacklist=2 multibuild=false ]

export_exlib_phases pkg_setup src_prepare

SUMMARY="KDE's crash handler"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="systemd [[ description = [ Coredumpd integration ] ]]"

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        systemd? ( virtual/pkg-config )
    build+run:
        kde-frameworks/kconfig:6[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:6[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:6[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:6[>=${KF5_MIN_VER}]
        kde-frameworks/kidletime:6[>=${KF5_MIN_VER}]
        kde-frameworks/kio:6[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:6[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:6[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:6[>=${KF5_MIN_VER}]
        kde-frameworks/kwallet:6[>=${KF5_MIN_VER}]
        kde-frameworks/kstatusnotifieritem:6[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:6[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:6[>=${KF5_MIN_VER}]
        kde-frameworks/syntax-highlighting:6[>=${KF5_MIN_VER}]
        sys-apps/systemd[>=249]
        sys-auth/polkit-qt:1[providers:qt6]
        x11-libs/qtbase:6[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:6[>=${QT_MIN_VER}]
        systemd? ( sys-apps/systemd )
        !kde/plasma-workspace:4[<5.10.90] [[
            description = [ kde/drkonqi was split out from kde/plasma-workspace:4 ]
            resolution = uninstall-blocked-after
        ]]
    run:
        kde-frameworks/kirigami:6[>=${KF5_MIN_VER}]
        kde-frameworks/kitemmodels:6[>=${KF5_MIN_VER}] [[
            note = [ org.kde.kitemmodels ]
        ]]
        kde-frameworks/syntax-highlighting:6[>=${KF5_MIN_VER}] [[
            note = [ org.kde.syntaxhighlighting ]
        ]]
        sys-devel/gdb[>=12]
    test:
        dev-python/chai[python_abis:*(-)?]
        dev-python/psutil[python_abis:*(-)?]
        dev-python/pygdbmi[python_abis:*(-)?]
        dev-python/sentry-sdk[python_abis:*(-)?]
"

if ever at_least 6.0.90 ; then
    :
else
    DEPENDENCIES+="
        run:
            kde-frameworks/kcmutils:6[>=${KF5_MIN_VER}] [[
                note = [ org.kde.kcmutils 1.0 ]
            ]]
    "
fi


CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DPython3_EXECUTABLE:PATH=${PYTHON}
    # TODO: integration tests would need ruby, at-spi2-core, atspi and xmlrpc
    #       gems (both unwritten), gdb, and xvfb-run.
    -DWITH_DRKONI_INTEGRATION_TESTING:BOOL=FALSE
    -DWITH_GDB12:BOOL=TRUE
    -DWITH_PYTHON_VENDORING:BOOL=FALSE
)
if ever at_least 6.2.91 ; then
    CMAKE_SRC_CONFIGURE_OPTION_WITHS+=( SYSTEMD )
else
    CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( Systemd )
fi

# connectiontest: Needs dbus, kdeinit5 and connects to komaci.kde.org
# preambletest: Makes some assumptions about the system which aren't true for
#               us
# backtraceparsertest: Fails with hardened_malloc
CMAKE_SRC_TEST_PARAMS+=(
    -E '(connectiontest|preambletest|backtraceparsertest)'
)

drkonqi_pkg_setup() {
    # Apparently something's wrong with sentry-sdk, eventlet and newer
    # dnspython versions:
    # https://groups.google.com/g/linux.debian.bugs.dist/c/gXD9iqjPzwo
    # It's here instead of src_test to make it work for
    # find_pythonmodule(sentry_sdk) during src_configure too.
    export EVENTLET_NO_GREENDNS=yes
}

drkonqi_src_prepare() {
    cmake_src_prepare

    edo sed \
        -e "s|/usr/bin/true|/usr/$(exhost --target)/bin/gtrue|" \
        -e "s|/usr/lib/x86_64-linux-gnu/|/usr/$(exhost --target)/lib/|" \
        -i src/tests/preambletest.py
}

