# Copyright 2019-2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require plasma kde [ kf_major_version=${major_version} translations=ki18n ]
require test-dbus-daemon

export_exlib_phases src_prepare

SUMMARY="Plasma system settings and kded modules to handle Thunderbolt devices"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build+run:
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcmutils:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}]
    run:
        app-admin/bolt
"

if [[ ${major_version} == 6 ]] ; then
    :
else
    DEPENDENCIES+="
        build:
            kde-frameworks/kpackage:${major_version}[>=${KF5_MIN_VER}]
        build+run:
            kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
        run:
            x11-libs/qtquickcontrols2:5[>=${QT_MIN_VER}]
    "
fi

plasma-thunderbolt_src_prepare() {
    kde_src_prepare

    # Disable kdedtest, which needs a running X server
    edo sed -e "/^add_subdirectory(kded)/d" -i autotests/CMakeLists.txt
}

