# Copyright 2008, 2009, 2012 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org kde [ translations='ki18n' ] gtk-icon-cache

SUMMARY="An advanced twin panel (commander style) file manager"
DESCRIPTION="
Krusader is an advanced twin-panel (commander-style) file-manager for KDE,
but with many extras. It provides all the file-management features you could
possibly want. It also features extensive archive handling, mounted filesystem
support, FTP, an advanced search module, a text viewer/editor, directory
synchronization, support for file content comparisons, powerful batch renaming,
and much more. It supports the following archive formats: tar, zip, bzip2,
gzip, rar, ace, arj, and rpm. It can also handle other KIOSlaves such as smb://
or fish://.
"
HOMEPAGE="https://www.krusader.org/"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( linguas: ast bg bs ca ca@valencia cs da de el en_GB eo es et eu fi fr ga gl hr hu ia it ja
               ko lt mai mr nb nds nl nn pa pl pt pt_BR ro ru sk sl sr sr@ijekavian
               sr@ijekavianlatin sr@latin sv tr ug uk zh_CN zh_TW )
"

DEPENDENCIES="
    build:
        kde-frameworks/extra-cmake-modules[>=1.7.0]
        kde-frameworks/kdoctools:5
    build+run:
        kde-frameworks/karchive:5
        kde-frameworks/kbookmarks:5
        kde-frameworks/kcodecs:5
        kde-frameworks/kcompletion:5
        kde-frameworks/kconfig:5
        kde-frameworks/kconfigwidgets:5
        kde-frameworks/kcoreaddons:5
        kde-frameworks/kguiaddons:5
        kde-frameworks/ki18n:5
        kde-frameworks/kiconthemes:5
        kde-frameworks/kio:5[>=5.23.0]
        kde-frameworks/kitemviews:5
        kde-frameworks/kjobwidgets:5
        kde-frameworks/knotifications:5
        kde-frameworks/kparts:5
        kde-frameworks/kservice:5
        kde-frameworks/ktextwidgets:5
        kde-frameworks/kwallet:5
        kde-frameworks/kwidgetsaddons:5
        kde-frameworks/kwindowsystem:5
        kde-frameworks/kxmlgui:5
        kde-frameworks/solid:5
        sys-apps/acl
        sys-apps/attr
        sys-libs/zlib
        x11-libs/qtbase:5[>=5.9.0]
    suggestion:
        app-misc/krename [[ description = [ Renaming of multiple files ] ]]
        dev-util/kdiff3  [[ description = [ Compare files within krusader ] ]]
        kde/kget:4 [[
            description = [ Synchronisation of local and remote folders ]
        ]]
"

