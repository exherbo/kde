# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf5_min_ver
myexparam qt_min_ver

require plasma kde [ kf_major_version=6 translations=ki18n ]

SUMMARY="A friendly onboarding wizard for Plasma"

LICENCES="
    CC0 [[ note = [ dot, desktop, appstream and similar files ] ]]
    || ( GPL-2 GPL-3 )
"
SLOT="0"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build+run:
        kde/kaccounts-integration:4[>=24.01.85]
        kde-frameworks/kcmutils:6[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:6[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:6[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:6[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:6[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:6[>=${KF5_MIN_VER}]
        kde-frameworks/kio:6[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:6[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:6[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:6[>=${KF5_MIN_VER}]
        x11-libs/qtbase:6[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:6[>=${QT_MIN_VER}]
        x11-libs/qtsvg:6[>=${QT_MIN_VER}]
    run:
        kde/kirigami-addons[>=1.2.0]
        kde/libplasma[>=${PV}]
        kde-frameworks/kirigami:6[>=${KF5_MIN_VER}]
        kde-frameworks/knewstuff:6[>=${KF5_MIN_VER}]   [[ note = [ launchApp(org.kde.knewstuff-dialog) ] ]]
        kde-frameworks/ksvg:6[>=${KF5_MIN_VER}]
        kde-frameworks/kuserfeedback:6[>=${KF5_MIN_VER}]
"

