# Copyright 2015-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps kde [ kf_major_version=${major_version} translations='ki18n' ]
require xdummy [ phase=test ] test-dbus-daemon

export_exlib_phases src_test

SUMMARY="An application to enable/disable qCDebug"

LICENCES="GPL-2"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build+run:
        kde-frameworks/kcompletion:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
"

if ever at_least 24.07.80 ; then
    DEPENDENCIES+="
        build+run:
            kde-frameworks/kiconthemes:6[>=${KF5_MIN_VER}]
    "

    CMAKE_SRC_CONFIGURE_PARAMS+=(
        # Experimental
        -DBUILD_QUICK_VERSION:BOOL=FALSE
        # SeleniumWebDiverATSPI is unpackaged
        -DOPTION_SELENIUMWEBDRIVER_SUPPORT:BOOL=FALSE
    )
else
    DEPENDENCIES+="
        build+run:
            kde-frameworks/kconfigwidgets:${major_version}[>=${KF5_MIN_VER}]
            kde-frameworks/kguiaddons:${major_version}[>=${KF5_MIN_VER}]
            kde-frameworks/kitemviews:${major_version}[>=${KF5_MIN_VER}]
            kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
    "
fi

kdebugsettings_src_test() {
    xdummy_start

    test-dbus-daemon_run-tests cmake_src_test

    xdummy_stop
}

