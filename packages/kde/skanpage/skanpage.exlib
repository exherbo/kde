# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps [ group_or_user=utilities ]
require kde [ kf_major_version=${major_version} translations='ki18n' ]
require gtk-icon-cache

SUMMARY="A multi-page scanning application using a QML interface"

LICENCES="
    BSD-2 [[ note = [ cmake scripts ] ]]
    CC0   [[ note = [ desktop files, appstream, etc ] ]]
    || ( GPL-2 GPL-3 )
"
SLOT="0"
MYOPTIONS="
    ocr [[ description = [ Support for OCR using tesseract/leptonica ] ]]
"

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build+run:
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/purpose:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}]
        ocr? (
            app-text/tesseract[>=4]
            media-libs/leptonica
        )
"

if ever at_least 24.01.90 ; then
    DEPENDENCIES+="
        build+run:
            kde/ksanecore:6[>=$(ever range -2)]
            kde-frameworks/kirigami:6[>=${KF5_MIN_VER}]
        run:
            kde/kquickimageeditor[>=0.3]
    "
else
    DEPENDENCIES+="
        build+run:
            kde/ksanecore:0[>=$(ever range -2)]
            kde-frameworks/kirigami:2[>=${KF5_MIN_VER}]
            x11-libs/qtquickcontrols2:5[>=${QT_MIN_VER}]
        run:
            kde/kquickimageeditor[<0.3]
    "
fi

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'ocr Leptonica'
    'ocr Tesseract'
)

