# Copyright 2013, 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps kde [ kf_major_version=${major_version} translations=k1i8n ]
require gtk-icon-cache

SUMMARY="World domination strategy game"
DESCRIPTION="
A computerized version of the well known strategic board game Risk. The goal of the game is
simply to conquer the world by attacking your neighbors with your armies."
HOMEPAGE+=" https://apps.kde.org/${PN}/"

LICENCES="GPL-2 FDL-1.2 LGPL-2.1"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:${major_version}[>=${KF5_MIN_VER}]
    build+run:
        kde-frameworks/kcompletion:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/knewstuff:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:${major_version}[>=${KF5_MIN_VER}]
        sys-libs/zlib
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
        x11-libs/qtsvg:${major_version}[>=${QT_MIN_VER}]
"
# NOTE: contains a copy of iris (http://delta.affinix.com/iris/) which has no
# releases so far, but since 24.01.x we disable this (see below)

if ever at_least 24.07.80 ; then
    DEPENDENCIES+="
        build+run:
            kde/libkdegames:5[>=24.01.75] [[ note = [ aka 6.0.0 ] ]]
            kde-frameworks/kdbusaddons:${major_version}[>=${KF5_MIN_VER}]
            x11-libs/qt5compat:6[>=${QT_MIN_VER}]
            x11-libs/qtmultimedia:${major_version}[>=${QT_MIN_VER}]
    "
else
    DEPENDENCIES+="
        build+run:
            app-crypt/qca:2[>=2.1.0][providers:qt5] [[ note = [ Build Ksirk Jabber support's GroupWise and Jabberprotocols ] ]]
            kde/libkdegames:5[>=21.04.0&<24] [[ note = [ aka 7.3.0 ] ]]
            kde-frameworks/kio:5[>=${KF5_MIN_VER}]
            kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
            media-libs/phonon[providers:qt5(+)]
    "
fi

