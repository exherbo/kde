# Copyright 2025 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org kde [ kf_major_version=6 ]
require gtk-icon-cache

SUMMARY="A simple application to scan many barcode formats and create QR code images"

LICENCES="
    BSD-2 [[ note = [ cmake ] ]]
    BSD-3 [[ note = [ gradle things ] ]]
    CC0   [[ note = [ dot files ] ]]
    GPL-2
    GPL-3
    LGPL-2.0
    LGPL-2.1
"
SLOT="0"
MYOPTIONS=""

KF6_MIN_VER="6.6.0"
QT_MIN_VER="6.6.0"

DEPENDENCIES="
    build+run:
        kde-frameworks/kconfig:6[>=${KF6_MIN_VER}]
        kde-frameworks/kcontacts:6[>=${KF6_MIN_VER}]
        kde-frameworks/kcoreaddons:6[>=${KF6_MIN_VER}]
        kde-frameworks/kcrash:6[>=${KF6_MIN_VER}]
        kde-frameworks/ki18n:6[>=${KF6_MIN_VER}]
        kde-frameworks/kio:6[>=${KF6_MIN_VER}]
        kde-frameworks/kirigami:6[>=${KF6_MIN_VER}]
        kde-frameworks/knotifications:6[>=${KF6_MIN_VER}]
        kde-frameworks/kservice:6[>=${KF6_MIN_VER}]
        kde-frameworks/networkmanager-qt:6[>=${KF6_MIN_VER}]
        kde-frameworks/prison:6[>=${KF6_MIN_VER}]
        kde-frameworks/purpose:6[>=${KF6_MIN_VER}]
        x11-libs/qtbase:6[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:6[>=${QT_MIN_VER}]
        x11-libs/qtmultimedia:6[>=${QT_MIN_VER}]
        x11-libs/qtsvg:6[>=${QT_MIN_VER}]
"

