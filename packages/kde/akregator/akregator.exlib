# Copyright 2016-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps kde [ kf_major_version=${major_version} translations='ki18n' ]
require freedesktop-desktop gtk-icon-cache

export_exlib_phases src_test pkg_postinst pkg_postrm

SUMMARY="A KDE news feed reader"

LICENCES="FDL-1.2 GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS="
    tts [[ description = [ Support for text to speech ] ]]
    userfeedback [[
        description = [ Allows sending anonymized usage information to KDE ]
    ]]
"

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:${major_version}[>=${KF5_MIN_VER}]
    build+run:
        kde-frameworks/kcmutils:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcodecs:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/knotifyconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/syndication:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}][gui]
        x11-libs/qtwebengine:${major_version}[>=${QT_MIN_VER}]
"

if ever at_least 24.11.80 ; then
    MYOPTIONS+="
        activities [[ description = [ Support for Activities of Plasma ] ]]
    "

    DEPENDENCIES+="
        build+run:
            activities? ( kde/pimcommon[activities][>=${PV}] )
            kde/grantleetheme:6[>=${PV}]
            kde/ktextaddons:6[>=1.5.4]
            kde/libkdepim[>=${PV}]
            kde/messagelib[>=${PV}]
            kde/pimcommon[>=${PV}]
            kde-frameworks/akonadi-mime:5[>=${PV}]
            kde-frameworks/kcolorscheme:6[>=${KF5_MIN_VER}]
            kde-frameworks/kiconthemes:6[>=${KF5_MIN_VER}]
            kde-frameworks/kontactinterface:6[>=${PV}]
            kde-frameworks/kstatusnotifieritem:6[>=${KF5_MIN_VER}]
            tts? ( kde/ktextaddons:6[>=1.5.2][tts] )
            userfeedback? ( kde-frameworks/kuserfeedback:6[>=${KF5_MIN_VER}] )
        suggestion:
            kde/kdepim-addons:4[>=24.01.95] [[ description = [ Provides an adblock plugin ] ]]
    "

    CMAKE_SRC_CONFIGURE_PARAMS+=(
        # Experimental
        -DOPTION_USE_PLASMA_ACTIVITIES:BOOL=FALSE
    )
    CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
        'activities KPim6PimCommonActivities'
        'userfeedback KF6UserFeedback'
    )
else
    DEPENDENCIES+="
        build+run:
            kde/grantleetheme:0[>=${PV}&<24]
            kde/libkdepim[>=${PV}&<24]
            kde/messagelib[>=${PV}&<24]
            kde/pimcommon[>=${PV}&<24]
            kde-frameworks/akonadi-mime:5[>=${PV}&<24]
            kde-frameworks/kontactinterface:5[>=${PV}&<24]
            kde-frameworks/kpimtextedit:${major_version}[>=${PV}&<24]
            kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
            tts? ( kde/ktextaddons:0[>=1.0.0][tts] )
            userfeedback? ( kde/kuserfeedback[>=1.2.0] )
        suggestion:
            kde/kdepim-addons:4[<24] [[ description = [ Provides an adblock plugin ] ]]
    "

    CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'userfeedback KUserFeedback' )
fi

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    "tts KF${major_version}TextEditTextToSpeech"
)

akregator_src_test() {
    ever at_least 24.07.80 && export QT_QPA_PLATFORM=vnc:port=0

    cmake_src_test
}

akregator_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

akregator_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

