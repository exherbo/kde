# Copyright 2019-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps kde [ kf_major_version=${major_version} translations=ki18n ]

SUMMARY="Library to assist with accessing information about public transport"
DESCRIPTION="
It can access realtime public transport data and perform public transport
journey queries and supports the following operations:

* Location searches: retrieve stop information based on name or geo
  coordinates.
* Arrival/departure queries: retrieve base schedule and if available realtime
  information
  about upcoming arrivals and departures at a stop.
* Journey queries: obtain ways to get from A to B.

All data is retrieved from online backends, this library is not offline
capable. The primary backend is https://navitia.io, which is Free Software and
relies on Open Data. Support for proprietary/vendor-specific APIs exists too
though.
"

LICENCES="
    BSD-3 [[ note = [ cmake scripts ] ]]
    CC-0  [[ note = [ data, scripts, and text files ] ]]
    LGPL-2.0 [[ note = [ src/osm/pbf/{file,osm}format.proto ] ]]
"
SLOT="0"
MYOPTIONS="
    doc [[ description = [ Build API docs in Qt's help format (.qch) ] ]]
    onboard-status [[ description = [ Allows detecting train WIFIs and queries about the journey ] ]]
"

exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        doc? (
            app-doc/doxygen
            x11-libs/qttools:${major_version}   [[ note = qhelpgenerator ]]
        )
    build+run:
        dev-libs/protobuf:=
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}]
        sys-libs/zlib
        onboard-status? ( kde-frameworks/networkmanager-qt:${major_version} )
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # "Needed only for regenereating line metadata tables (ie. you most likely
    # don't need this)"
    -DCMAKE_DISABLE_FIND_PACKAGE_OsmTools:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'doc QCH' )
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    "onboard-status KF${major_version}NetworkManagerQt"
)

