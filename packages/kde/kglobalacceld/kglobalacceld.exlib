# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf5_min_ver
myexparam qt_min_ver

require plasma kde [ kf_major_version=6 translations='ki18n' ]
require test-dbus-daemon

export_exlib_phases src_test

SUMMARY="Daemon providing Global Keyboard Shortcut (Accelerator) functionality"

LICENCES="
    CC0
    LGPL-2.0
    || ( LGPL-2.1 LGPL-3.0 ) [[ note = [ src/logging* ] ]]
"
SLOT="0"
MYOPTIONS="X"

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kglobalaccel:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
        !kde-frameworks/kglobalaccel:5[runtime] [[
            description = [ The runtime components collide with the Qt5/KF5 package ]
            resolution = uninstall-blocked-after
        ]]
        X? (
            x11-libs/libX11
            x11-libs/libxcb
            x11-utils/xcb-util-keysyms
        )
"

# Tests pass but hang afterwards
RESTRICT="test"

CMAKE_SRC_CONFIGURE_OPTION_WITHS+=(
    'X X11'
)

CMAKE_SRC_TEST_PARAMS+=(
    # TODO: passes on upstream CI, but
    # "MigrateConfigTest::testMigrate() Compared lists have different sizes.":
    # 12 != 13
    -E migrateconfigtest
)

kglobalacceld_src_test() {
    export QT_QPA_PLATFORM=vnc:port=0
    test-dbus-daemon_run-tests cmake_src_test
}

