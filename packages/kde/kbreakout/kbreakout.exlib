# Copyright 2013, 2015-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps kde [ kf_major_version=${major_version} translations=k1i8n ]
require gtk-icon-cache

SUMMARY="Breakout-like game"
DESCRIPTION="
Its object is to destroy as many bricks as possible without losing the ball."
HOMEPAGE+=" https://apps.kde.org/${PN}/"

LICENCES="GPL-2 FDL-1.2"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:${major_version}[>=${KF5_MIN_VER}]
    build+run:
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}]
"

if ever at_least 24.01.85 ; then
    DEPENDENCIES+="
        build+run:
            kde/libkdegames:5[>=24.01.75]
    "

    CMAKE_SRC_CONFIGURE_PARAMS+=(
        # TODO: Add app-arch/p7zip but the 7zip versions are confusing and
        # Find7Zip module doesn't work with our p7zip yet; falls back to gzip
        -DCMAKE_DISABLE_FIND_PACKAGE_7Zip:BOOL=TRUE
    )
else
    DEPENDENCIES+="
        build+run:
            kde/libkdegames:5[>=21.03.80&<24]
    "
fi

