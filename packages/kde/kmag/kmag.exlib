# Copyright 2012, 2017-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'kdeaccessibility.exlib', which is:
#     Copyright 2010 Yury G. Kudryashov and 2010-2011 Bo Ørsted Andresen

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps kde [ kf_major_version=${major_version} translations=ki18n ]
require gtk-icon-cache

SUMMARY="Screen Magnifier"
DESCRIPTION="A small utility for Linux to magnify a part of the screen. KMag is very useful for
people with visual disabilities and for those working in the fields of image analysis, web
development etc."

HOMEPAGE+=" https://www.kde.org/applications/utilities/${PN}/"

LICENCES="GPL-2 FDL-1.2"
MYOPTIONS="
    keyboard-focus [[ description = [ Automatically magnify the place where the cursor is ] ]]
"

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:${major_version}[>=${KF5_MIN_VER}]
    build+run:
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
"

if ever at_least 24.01.75 ; then
    DEPENDENCIES+="
        build+run:
            keyboard-focus? ( dev-libs/libqaccessibilityclient:6[>=0.4.0] )
    "

    CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
        'keyboard-focus QAccessibilityClient6'
    )
else
    DEPENDENCIES+="
        build+run:
            keyboard-focus? ( dev-libs/libqaccessibilityclient:0[>=0.4.0] )
    "

    CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
        'keyboard-focus QAccessibilityClient'
    )
fi


