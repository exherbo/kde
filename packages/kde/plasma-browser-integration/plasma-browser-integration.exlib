# Copyright 2017-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require plasma kde [ kf_major_version=${major_version} translations='ki18n' ]

export_exlib_phases pkg_postinst

SUMMARY="Integrate browsers into Plasma desktop"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build+run:
        kde/plasma-workspace:4
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kfilemetadata:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/krunner:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/purpose:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
"

if ever at_least 5.91.0 ; then
    DEPENDENCIES+="
        build+run:
            kde/plasma-activities[>=${PV}]
            kde-frameworks/kstatusnotifieritem:${major_version}[>=${KF5_MIN_VER}]
    "
else
    DEPENDENCIES+="
        build+run:
            kde-frameworks/kactivities:${major_version}[>=${KF5_MIN_VER}]
    "
fi

DEFAULT_SRC_CONFIGURE_PARAMS+=(
    # Would install a locally built extension system-wide
    -DINSTALL_CHROME_EXTENSION:BOOL=FALSE
    # Would install a configuration file that makes Chrome automatically
    # download the extension from the store
    -DINSTALL_CHROME_MANIFEST:BOOL=FALSE
)

plasma-browser-integration_pkg_postinst() {
    elog "This package only installs a native messaging host. You also need to"
    elog "download the extension for your browser:"
    elog ""
    elog " - Chrome: https://chrome.google.com/webstore/detail/plasma-integration/cimiefiiaegbelhefglklhhakcgmhkai"
    elog " - Firefox: https://addons.mozilla.org/de/firefox/addon/plasma-integration/"
}

