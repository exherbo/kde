# Copyright 2016 Niels Ole Salscheider <olesalscheider@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps kde [ kf_major_version=${major_version} translations=ki18n ]
require python [ blacklist='2' multibuild=false ]

SUMMARY="Python 3 plugin for KDevelop"

LICENCES="GPL-2 LGPL-2"
SLOT="4"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build+run:
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ktexteditor:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/threadweaver:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
"

if ever at_least 24.07.80 ; then
    DEPENDENCIES+="
        build+run:
            kde/kdevelop:4[>=${PV}]
            kde-frameworks/kconfig:6[>=${KF5_MIN_VER}]
            kde-frameworks/kcoreaddons:6[>=${KF5_MIN_VER}]
            kde-frameworks/kparts:6[>=${KF5_MIN_VER}]
            kde-frameworks/kservice:6[>=${KF5_MIN_VER}]
            kde-frameworks/kwidgetsaddons:6[>=${KF5_MIN_VER}]
            kde-frameworks/kxmlgui:6[>=${KF5_MIN_VER}]
            x11-libs/qt5compat:6[>=${QT_MIN_VER}]
    "
else
    DEPENDENCIES+="
        build+run:
            kde/kdevelop:4[>=${PV}&<24.07]
    "
fi

# tests fail because they need an Xserver
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DPYTHON_EXECUTABLE:PATH=${PYTHON}
)

