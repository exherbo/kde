# Copyright 2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps kde [ kf_major_version=${major_version} translations=ki18n ]
require gtk-icon-cache

SUMMARY="Kirigami YouTube video player based on QtMultimedia and youtube-dl"

LICENCES="
    CC0
    CCPL-Attribution-ShareAlike-4.0
    GPL-3
"
SLOT="0"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build+run:
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}]
        x11-libs/qtsvg:${major_version}[>=${QT_MIN_VER}]
    run:
        net-misc/youtube-dl
        x11-libs/qtmultimedia:${major_version}[>=${QT_MIN_VER}] [[ note = [ import QtMultimedia ] ]]
"

if ever at_least 24.04.80 ; then
    DEPENDENCIES+="
        build+run:
            media-libs/mpvqt
            kde-frameworks/kdbusaddons:${major_version}[>=${KF5_MIN_VER}]
            kde-frameworks/kwindowsystem:${major_version}[>=${KF5_MIN_VER}]
            sys-auth/qtkeychain[providers:qt6]
        run:
            kde/kirigami-addons:6[>=1.1.0]
            kde-frameworks/kirigami:${major_version}[>=${KF5_MIN_VER}]
            kde-frameworks/purpose:${major_version}[>=${KF5_MIN_VER}]
    "
else
    DEPENDENCIES+="
        build:
            virtual/pkg-config
        build+run:
            media/mpv
            x11-libs/qtquickcontrols2:5[>=${QT_MIN_VER}]
        run:
            kde-frameworks/kirigami:2[>=${KF5_MIN_VER}]
    "
fi

