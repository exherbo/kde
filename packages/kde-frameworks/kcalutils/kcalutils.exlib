# Copyright 2014-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps kde [ kf_major_version=${major_version} translations='ki18n' ]
require xdummy [ phase=test ] test-dbus-daemon

export_exlib_phases src_test

SUMMARY="A library providing utility functions for the handling of calendar data"

LICENCES="LGPL-2"
MYOPTIONS="
    doc [[ description = [ Build API docs in Qt's help format (.qch) ] ]]
"

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        doc? (
            app-doc/doxygen
            x11-libs/qttools:${major_version}   [[ note = qhelpgenerator ]]
        )
    build+run:
        kde-frameworks/kcalendarcore:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcodecs:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kidentitymanagement:${major_version}[>=${PV}]
        kde-frameworks/kwidgetsaddons:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
"

if ever at_least 24.01.95 ; then
    DEPENDENCIES+="
        build+run:
            kde-frameworks/ktexttemplate:${major_version}[>=${KF5_MIN_VER}]
    "
else
    DEPENDENCIES+="
        build+run:
            dev-libs/grantlee:${major_version}[>=5.2]
    "
fi

CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'doc QCH' )

# Skip tests which also fail on b.k.o, eg.
# https://build.kde.org/job/Applications/job/kcalutils/job/stable-kf5-qt5%20SUSEQt5.12/34/
CMAKE_SRC_TEST_PARAMS+=( -E '(testtodotooltip|testincidenceformatter)' )

kcalutils_src_test() {
    xdummy_start

    test-dbus-daemon_run-tests cmake_src_test

    xdummy_stop
}

