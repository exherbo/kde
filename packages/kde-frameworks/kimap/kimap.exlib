# Copyright 2014-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps kde [ kf_major_version=${major_version} translations='ki18n' ]
require test-dbus-daemon

SUMMARY="A job-based API for interacting with IMAP servers"
DESCRIPTION="
This library provides a job-based API for interacting with an IMAP4rev1
server. It manages connections, encryption and parameter quoting and encoding,
but otherwise provides quite a low-level interface to the protocol. This
library does not implement an IMAP client; it merely makes it easier to do so."

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] GPL-2 LGPL-2.1"
MYOPTIONS="
    doc [[ description = [ Build API docs in Qt's help format (.qch) ] ]]
"

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        doc? (
            app-doc/doxygen
            x11-libs/qttools:${major_version}   [[ note = qhelpgenerator ]]
        )
    build+run:
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kmime:${major_version}[>=${PV}]
        net-libs/cyrus-sasl
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
"

CMAKE_SRC_CONFIGURE_PARAMS+=( -DBUILD_WITH_COMPAT_LIBS:BOOL=TRUE )

if ever at_least 24.01.90 ; then
    :
else
    CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'doc QCH' )
fi

