# Copyright 2014-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
myexparam kf5_min_ver
myexparam qt_min_ver

exparam -v major_version kf_major_version

require kde-apps kde [ kf_major_version=${major_version} translations='ki18n' ]
require xdummy [ phase=test ] test-dbus-daemon

export_exlib_phases src_test

SUMMARY="Manage PIM identity"

LICENCES="LGPL-2.1"
MYOPTIONS="
    doc [[ description = [ Build API docs in Qt's help format (.qch) ] ]]
"

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        doc? (
            app-doc/doxygen
            x11-libs/qttools:${major_version}   [[ note = qhelpgenerator ]]
        )
    build+run:
        kde-frameworks/kcodecs:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kpimtextedit:${major_version}[>=${PV}]
        kde-frameworks/kwidgetsaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtbase:${major_version}[>=${QT_MIN_VER}]
"

if ever at_least 24.04.80 ; then
    MYOPTIONS+="
        qml [[ description = [ Support for QtQuick and the QML language ] ]]
    "

    DEPENDENCIES+="
        build+run:
            kde/ktextaddons:6[>=1.5.4]
            qml? ( x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}] )
        run:
            qml? ( kde/kirigami-addons:6[>=1.0] )
    "

    CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'qml QUICK_SUPPORT' )
else
    DEPENDENCIES+="
        build+run:
            kde-frameworks/ktextwidgets:${major_version}[>=${KF5_MIN_VER}]
    "
fi

CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'doc QCH' )

# Skip test which also fails on build.de.org
CMAKE_SRC_TEST_PARAMS+=( -E signaturetest )

kidentitymanagement_src_test() {
    xdummy_start

    cmake_src_test

    xdummy_stop
}

