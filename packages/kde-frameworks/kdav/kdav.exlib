# Copyright 2017-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
exparam -v major_version kf_major_version

require kde-frameworks [ docs=true ] kde [ kf_major_version=${major_version} translations='ki18n' ]
require xdummy [ phase=test ] test-dbus-daemon

export_exlib_phases src_test

SUMMARY="A DAV protocol implementation with KJobs"
DESCRIPTION="
Calendars and todos are supported, using either GroupDAV
or CalDAV, and contacts are supported using GroupDAV or
CardDAV."

LICENCES="GPL-2"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
"

# davitemfetchjob wants to start a http ioslave, davcollectionsmulti... just fail
CMAKE_SRC_TEST_PARAMS+=(
    -E '(davitemfetchjob|davcollectionsmultifetchjobtest)'
)

kdav_src_test() {
    xdummy_start

    test-dbus-daemon_run-tests cmake_src_test

    xdummy_stop
}

