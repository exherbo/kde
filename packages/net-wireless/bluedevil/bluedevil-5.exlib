# Copyright 2015 Timo Gurr <tgurr@exherbo.org>
# Copyright 2018-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf5_min_ver
myexparam qt_min_ver

require plasma kde [ kf_major_version=5 translations='ki18n' ]
require freedesktop-desktop freedesktop-mime

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="Integrates the Bluetooth technology within KDE workspace and applications"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        kde-frameworks/bluez-qt:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/plasma-framework:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-misc/shared-mime-info
    run:
        kde/kde-cli-tools:4 [[ note = [ kcmshell5 ] ]]
        kde-frameworks/kirigami:2[>=${KF5_MIN_VER}]
    recommendation:
        net-wireless/bluez[obex] [[ description = [ Support browsing, sending and receiving files ] ]]
    suggestion:
        media-sound/pulseaudio[bluetooth] [[ description = [ Support connections via the A2DP profile ] ]]
"

bluedevil-5_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
}

bluedevil-5_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
}

