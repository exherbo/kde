# Copyright 2015 Timo Gurr <tgurr@exherbo.org>
# Copyright 2018-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf6_min_ver
myexparam qt_min_ver

require plasma kde [ kf_major_version=6 translations='ki18n' ]
require freedesktop-desktop freedesktop-mime

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="Integrates the Bluetooth technology within KDE workspace and applications"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

exparam -v KF6_MIN_VER kf6_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:6[>=${KF6_MIN_VER}]
    build+run:
        kde/libplasma[>=$(ever range -3)]
        kde-frameworks/bluez-qt:6[>=${KF6_MIN_VER}]
        kde-frameworks/kcmutils:6[>=${KF6_MIN_VER}]
        kde-frameworks/kconfig:6[>=${KF6_MIN_VER}]
        kde-frameworks/kconfigwidgets:6[>=${KF6_MIN_VER}]
        kde-frameworks/kcoreaddons:6[>=${KF6_MIN_VER}]
        kde-frameworks/kdbusaddons:6[>=${KF6_MIN_VER}]
        kde-frameworks/ki18n:6[>=${KF6_MIN_VER}]
        kde-frameworks/kio:6[>=${KF6_MIN_VER}]
        kde-frameworks/kjobwidgets:6[>=${KF6_MIN_VER}]
        kde-frameworks/knotifications:6[>=${KF6_MIN_VER}]
        kde-frameworks/kservice:6[>=${KF6_MIN_VER}]
        kde-frameworks/ksvg:6[>=${KF6_MIN_VER}]
        kde-frameworks/kwidgetsaddons:6[>=${KF6_MIN_VER}]
        kde-frameworks/kwindowsystem:6[>=${KF6_MIN_VER}]
        x11-libs/qtbase:6[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:6[>=${QT_MIN_VER}]
        x11-misc/shared-mime-info
    run:
        kde-frameworks/kirigami:6[>=${KF6_MIN_VER}]
    recommendation:
        net-wireless/bluez[obex] [[ description = [ Support browsing, sending and receiving files ] ]]
    suggestion:
        media-sound/pulseaudio[bluetooth] [[ description = [ Support connections via the A2DP profile ] ]]
"

bluedevil_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
}

bluedevil_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
}

