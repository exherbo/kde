# Copyright 2008, 2009, 2010, 2012 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2014-2015, 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
#
# Use this exlib for all packages using KDE's CMake buildsystem

# Keep in sync with cmake.exlib in ::arbor
myexparam cmake_minimum_version=3.24.0

require flag-o-matic cmake [ cmake_minimum_version=$(exparam cmake_minimum_version) ]

myexparam -b debug=true
myexparam -b dependencies=true
# Set to '5' for packages based on Qt5/KF5, don't really use anything else
# than the default yet, unless you know exactly what you're doing.
myexparam kf_major_version=5
# Three possible values for translations. The first two options expect the
# needed files in po/.
# - qt:
#       Qt based translations
#       Typically those are recognised by something in CMakeLists.txt like:
#          "include (ECMPoQmTools)
#          ecm_install_po_files_as_qm(po)"
# - ki18n:
#       Gettext based translation via KDE's internationalization system
#       Those usually include:
#          "ki18n_install"
#       in CMakeLists.txt
# - none:
#       Default, no translations included.
myexparam translations=none

export_exlib_phases pkg_setup src_prepare src_configure src_install

exparam -b debug &&
MYOPTIONS="debug"
exparam -v translation_type translations
exparam -v major_version kf_major_version

case ${major_version} in
    5)
        ECM_MIN_VERSION="5.116.0" ;;
    6)
        ECM_MIN_VERSION="6.11.0" ;;
    *)
        die "Only \"5\" or \"6\" are valid values for the kf_major version exparam"
esac

# This is a simplification, but probably every KDE project uses it, it's
# tiny with few dependencies and we usually only keep one Frameworks
# version (the latest) anyway. Avoids listing it everywhere or carrying
# an exparam with a mimimal version.
DEPENDENCIES="
    build:
        kde-frameworks/extra-cmake-modules[>=${ECM_MIN_VERSION}]
"

if [[ ${translation_type} == qt ]] ; then
    DEPENDENCIES+="
        build:
            x11-libs/qttools:${major_version} [[ note = [ lrelease/lconvert for translations ] ]]
        "
elif [[ ${translation_type} == ki18n && ${PN} != ki18n ]] ; then
    DEPENDENCIES+="
        build:
            kde-frameworks/ki18n:${major_version} [[ note = [ ki18n_install() from KF5I18NMacros.cmake ] ]]
            sys-devel/gettext
    "
fi

CMAKE_SRC_CONFIGURE_TESTS+=( '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE' )

_remove_localisations_from_dir() {
    local dir="${1}" lingua

    for lingua in $(ls "${dir}") ; do
        if [[ -d ${dir}/${lingua} ]] && ! has ${lingua} ${LINGUAS} ; then
            # Remove localised files installed by ecm_install_po_file_as_qm(),
            # ki18n_install() and and kdoctools_install() called from the
            # parent CMakeLists.txt
            edo rm -rf "${dir}/${lingua}"

            # Remove localised data files installed via ordinary install in
            # a CMakeLists.txt
            if [[ -f ${dir}/CMakeLists.txt ]] ; then
                edo sed \
                    -e "/add_subdirectory[[:space:]]*([[:space:]]*${lingua}.*/d" \
                    -i "${dir}"/CMakeLists.txt
            fi
        fi
    done
}

kde_src_prepare() {
    cmake_src_prepare

    if [[ ${translation_type} != none ]] ; then
        echo "Removing unselected translations..."
        for dir in po poqm ; do
            if [[ -d ${CMAKE_SOURCE}/${dir} ]] ; then
                _remove_localisations_from_dir ${dir}
            fi
        done
    fi
}

kde_pkg_setup() {
    exparam -b debug && option !debug && append-flags -DNDEBUG -DQT_NO_DEBUG
}


kf_common_cmake_params() {
    echo \
        -DBUILD_SHARED_LIBS:BOOL=TRUE \
        -DKDE_INSTALL_AUTOSTARTDIR=/etc/xdg/autostart \
        -DKDE_INSTALL_CONFDIR=/etc/xdg \
        -DKDE_INSTALL_DATAROOTDIR:PATH=/usr/share \
        -DKDE_INSTALL_LIBEXECDIR=libexec \
        -DKDE_INSTALL_SYSCONFDIR=/etc \
        -DKDE_INSTALL_USE_QT_SYS_PATHS:BOOL=TRUE
}

kf5_shared_cmake_params() {
    # The LIBEXECDIR is intentionally relative, as some paths are hardcoded
    # into scripts or programs. Giving absolute paths results in hardcoded
    # paths like /usr/usr/libexec, cf. kde-modules/KDEInstallDirs.cmake in
    # extra-cmake-modules.
    echo \
        $(kf_common_cmake_params) \
        -DQT_MAJOR_VERSION=5
}

kf6_shared_cmake_params() {
    echo \
        $(kf_common_cmake_params) \
        -DQT_MAJOR_VERSION=6
}

kde_src_configure() {

    if [[ ${major_version} == 6 ]] ; then
        CMAKE_SRC_CONFIGURE_PARAMS+=( $(kf6_shared_cmake_params) )
    else
        CMAKE_SRC_CONFIGURE_PARAMS+=( $(kf5_shared_cmake_params) )
    fi

    local p
    for p in ${MY_KDE_PARTS}; do
        CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( "kde_parts:${p}" )
    done

    cmake_src_configure
}

kde_src_install() {
    cmake_src_install

    if [[ ${translation_type} != none ]] ; then
        if [[ -d "${IMAGE}"/usr/share/locale ]] \
            && [[ -z $(ls -A "${IMAGE}"/usr/share/locale) ]] ; then
            edo find "${IMAGE}"/usr/share -type d -empty -delete
        fi
    fi
}

