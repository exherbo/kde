Title: syndication update needs --permit-downgrade
Author: Heiko Becker <heirecka@exherbo.org>
Content-Type: text/plain
Posted: 2018-09-08
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: kde-frameworks/syndication[>=15.08]

kde-frameworks/syndication was moved from KDE Applications to KDE Frameworks,
which use a different versioning scheme, resulting in the need to forcefully
downgrade the package (e.g. from 18.08.1 to 5.50.0). You can do that via:

# cave resolve -1 kde-frameworks/syndication --permit-downgrade kde-frameworks/syndication
